# README #

This is a code test

### What is this repository for? ###
Implement a method to convert a sequence of records into a hierarchical (tree) structure. The records contain unsorted, but equal number of elements in the following format:

[A11].[A12].[A13]…[A1n]
[A21].[A22].[A23]…[A2n]
…
[Am1].[Am2].[Am3]…[Amn]

[Aij] are String (alphanumeric) values separated by dots. 

For example, if a set of records looks like this:

1.2.3
1.2.5
1.5.6
1.2.4
2.5.6
1.5.7
2.5.3

the output should be:
1
--2
----3
----4
----5
--5
----6
----7
2
--5
----3
----6
*/
* Quick summary
* Version 1.0

