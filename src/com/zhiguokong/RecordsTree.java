package com.zhiguokong;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;


/*
Implement a method to convert a sequence of records into a hierarchical (tree) structure. The records contain unsorted, but equal number of elements in the following format:

[A11].[A12].[A13]…[A1n]
[A21].[A22].[A23]…[A2n]
…
[Am1].[Am2].[Am3]…[Amn]

[Aij] are String (alphanumeric) values separated by dots. 

For example, if a set of records looks like this:

1.2.3
1.2.5
1.5.6
1.2.4
2.5.6
1.5.7
2.5.3

the output should be:
1
--2
----3
----4
----5
--5
----6
----7
2
--5
----3
----6
*/

public class RecordsTree {
	
	 
	private Node<String> root;
	 

	//Constructor
	 public RecordsTree( ) {
		 this.root = new Node<String>("ROOT");
	 }
	 
	 public Node<String> getRoot() {
		 return this.root;
	 }
	 
	 public static void main(String[] args) {	

         RecordsTree recordsTree = new RecordsTree();
		 FileInputStream fis;
		 try {
			 fis = new FileInputStream(new File("input.txt"));
			 //Construct BufferedReader from InputStreamReader
			 BufferedReader br = new BufferedReader(new InputStreamReader(fis));

			 String record = null;
			 try {
				 while ((record = br.readLine()) != null) {
					 String delims = "[.]";
					 String[] tokens = record.split(delims);
					 recordsTree.addRecord(recordsTree.getRoot(), tokens, 0);
				 }
				 br.close();
			 } catch (IOException e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
			 }

		 } catch (FileNotFoundException e1) {
			 // TODO Auto-generated catch block
			 e1.printStackTrace();
		 }
		 recordsTree.printRecords(recordsTree.getRoot(), 0);
	 }
  
	 /*
	  * Adding record in a recursive manner
	  */
	 public void addRecord(Node<String> root, String[] data, int index) {
		Node<String> childFound = root.findChild(data[index]);
		//if no match at this level, create new child with the data
		if(childFound==null) {
			if(index==data.length-1) {
				Node<String> child = new Node<String>(data[index]);
				root.addChild(child);
			}
			else {
				Node<String> child = new Node<String>(data[index]);
				root.addChild(child);
				addRecord(child, data, ++index );
			} 
		}
		//find a match, move to next level
		else
			addRecord(childFound, data, ++index );
	 } 
	 
	 /*
	  * Print record in a recursive manner
	  */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void printRecords(Node<String> root, int level) {
		//add the tree level indicator --
		 for( int i = 0; i<level-1; i++) 
			  System.out.print("--"); 
		 
		 if(level>0) {
			 System.out.print(root.getData());
			 System.out.println();//terminate current line
		 }
		 
		 List<Node> children = root.getChildren();

		 if(! children.isEmpty()) {
		     //Sort children in ascending order
			 if(children.size()==1) 
				 printRecords(children.get(0), ++level);
			 else {
				 Collections.sort(children);
				 ++level;
				 for(Node<String> node : children){
					 printRecords(node, level);
				 }	
			 }
		 }
		 
	 }
	 

}
