package com.zhiguokong;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("rawtypes")
public class Node<T extends Comparable<? super T>> implements Comparable<Node<T>> {
	 private T data;
	 private List<Node> children;
	 
	 //Constructor
	 public Node(T data) {
		 this.data = data;
		 this.children = new ArrayList<Node>();
	 }
	 
	 public T getData( ) {
		 return this.data;
	 }
	 
	 public void setData( T data ) {
		 this.data = data;
	 }
	 
	 public List<Node> getChildren() {
		 return this.children;
	 }
	 	 
	 public void addChild(Node<T> child) {
		 if( findChild(child.data)==null )
			 this.children.add(child);
	 }
	 
	 public Node<T> findChild(T data) {
		 Node<T> target = null;
		 if(!children.isEmpty()){
			for( Node<T> child : children)
				 if(child.data.equals(data)) {
					 target = child; 
					 break;
				 }			 
		 } 
		 return target;
	 }
	 
	@SuppressWarnings("unchecked")
	@Override
	 public int compareTo(final Node other) {
			 return this.getData().compareTo((T)other.getData());
     }
	 
}
