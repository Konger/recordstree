package com.zhiguokong;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import junit.framework.Assert;

import org.junit.Test;

public class RecordsTreeTest {

	@Test
	public void testTreeConstructor() {
		RecordsTree recordsTree = new RecordsTree();
		Node<String> root = recordsTree.getRoot();
		Assert.assertEquals("failure - expected result content match", "ROOT", root.getData());
		Assert.assertEquals("failure - expected result content match", 0, root.getChildren().size());
	}
	
	@Test
	public void testWithAlphabets() {
		 RecordsTree recordsTree = new RecordsTree();
		 FileInputStream fis;
		 try {
			 fis = new FileInputStream(new File("input_alpha.txt"));
			 //Construct BufferedReader from InputStreamReader
			 BufferedReader br = new BufferedReader(new InputStreamReader(fis));

			 String record = null;
			 try {
				 while ((record = br.readLine()) != null) {
					 String delims = "[.]";
					 String[] tokens = record.split(delims);
					 recordsTree.addRecord(recordsTree.getRoot(), tokens, 0);
				 }
				 br.close();
			 } catch (IOException e) {
				 // TODO Auto-generated catch block
				 e.printStackTrace();
			 }

		 } catch (FileNotFoundException e1) {
			 // TODO Auto-generated catch block
			 e1.printStackTrace();
		 }
		 recordsTree.printRecords(recordsTree.getRoot(), 0);
	}
	

}
